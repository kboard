ADD_EXECUTABLE(test_weakset
  test_weakset.cpp)

TARGET_LINK_LIBRARIES(test_weakset
  -lboost_unit_test_framework)
